import {Card, Button, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({product}) {

	const {name, description, price, _id} = product;

	return (

      	<Col xs={12} md={6} lg={4} classnName="h-100 p-2 m-1 " >
	      	<Card className="h-100 p-3 bg-transparent">
	      	<Card.Body>
	      	<h2>{name}</h2>
	      	<Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <h6>Price: &#8369; {price}</h6> 
	      	</Card.Body>
	      	<Button className="button-properties" as={Link} to ={`/products/${_id}`}>Details</Button>
	      	</Card> 
      	</Col>

	)
}