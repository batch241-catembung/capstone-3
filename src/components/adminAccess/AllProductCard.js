import {Card, Button, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function AllProductCard({allproduct}) {

	const {name, description, price, isActive, _id} = allproduct;
	//	let isActiveString = isActive.toString();
	let isActiveString;
	if (isActive){ isActiveString="Available"; }
	else {isActiveString="Not Available"; }

	return (

      	<Col xs={12} md={6} lg={4} classnName="h-100 p-2 m-1 " >
	      	<Card className="h-100 p-3 bg-transparent">
	      	<Card.Body>
	      	<h2>{name}</h2>
	      	<Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <h6>Price: &#8369; {price}</h6>
	        {isActiveString === "Available" ?
	        <h6 className="active-color">{isActiveString}</h6>
	        :
	        <h6 className="notActive-color">{isActiveString}</h6>
	        }
	      	</Card.Body>
	      	<Button className="button-properties" as={Link} to ={`/allproduct/${_id}`}>Details</Button>
	      	</Card>
      	</Col>
 
	)
}