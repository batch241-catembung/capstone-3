import { useState, useEffect} from 'react';
import { Container, Button, Row, Col, Form } from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AllProductView() {

	const navigate = useNavigate();
	const{productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState('');


	const updateProduct = (e)=>{
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/products/updateProduct/${productId}`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				
				name: name,
				description: description,
				price: price,
				isActive: isActive
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			if (data.error) {
				Swal.fire({
					title: "Registration Failed",
					icon: "error",
					text: data.error,
				});
			} else {
				console.log(name, description, price, isActive)
				Swal.fire({
					title: "Successfully added",
					icon: "success",
					text: "You have successfully added it to the list",
				});
				navigate("/admin");
				setName('');
				setDescription('');
				setPrice('');
			}
		})
	}

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/retrieveOneProduct/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setIsActive(data.isActive);
		})
	},[productId])

	return (

		<Container className="vh-100 d-flex justify-content-center align-items-center ">
		<Container >

		<h1 className="color-gray spacingletters pb-3 text-center">Update Details of Product</h1>

		<Form onSubmit={(e) => updateProduct(e)}>
		<Form.Group controlId="ProductName" className="pb-1">
		<Form.Label>Product Name</Form.Label>
		<Form.Control 
		type="text" 
		placeholder="Enter the name of the product"
		value={name}
		onChange={e => setName(e.target.value)} 
		required
		/>
		</Form.Group>

		<Form.Group className="pb-1">
		<Form.Label>Description</Form.Label>
		<Form.Control as="textarea"
		placeholder="Details of the product" 
		value={description}
		onChange={e => setDescription(e.target.value)} 
		required />
		</Form.Group>

		<Row className="g-2 pb-1 ">
			<Col md>
			<Form.Group controlId="Price">
			<Form.Label>Price</Form.Label>
			<Form.Control 
			type="text" 
			placeholder="Enter product price"
			value={price}
			onKeyPress={(event) => {if (!/[0-9]/.test(event.key)) {event.preventDefault();}}}
			onChange={e => setPrice(e.target.value)} 
			required
			/>
			</Form.Group>
			</Col>

			<Col md>
			<Form.Group controlId="isActive">
			<Form.Label>Set Active</Form.Label>
			<Form.Select                    
			type="text" 
			placeholder="true or false"
			value={isActive}
			onChange={e => setIsActive(e.target.value)} 
			required>
			<option >true</option>
			<option value="false">false</option>
			</Form.Select>
			</Form.Group>
			</Col>
		</Row>

		<Row className="g-2 pb-1 mt-1">
			<Col md> 
			<p></p>
			</Col>

			<Col md>
			<Button  type="submit" id="submitBtn" className="button-properties  w-100">
			update
			</Button>
			</Col>

			<Col md>
			<p></p>
			</Col>
		</Row>


		</Form>

		</Container>
		</Container>


	)
}