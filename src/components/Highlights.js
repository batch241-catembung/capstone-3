import { Container, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';

import {useContext} from 'react';
import UserContext from '../UserContext';

export default function Highlights() {
    const { user } = useContext(UserContext);

  return (
    <Container className="vh-100 d-flex justify-content-center align-items-center">
      <Container className="BanneBox text-center p-5">
      	<h1 className="spacingletters font-weight-bold color-red">PEN SHOP</h1>
        <h4 className="color-gray">Always on quality</h4>
        <p className="m-3 color-gray">Welcome to PEN SHOP, your go-to destination for high-quality 
        ballpoint pens. Our commitment to quality ensures that every pen you purchase from 
        us will meet and exceed your expectations.</p>

            { (user.id !== null) ?
              <h1> </h1>
              :
              <>
              <Button className="button-properties" as={Link} to="/register">Sign up</Button>
              </>
            }

      </Container>
    </Container>
  );
}
