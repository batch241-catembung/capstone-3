import {useContext} from 'react';
import {NavLink} from 'react-router-dom';
import UserContext from '../UserContext';
import {Container, Navbar, Offcanvas, Nav} from 'react-bootstrap';

export default function AppNavbar() {

  const { user } = useContext(UserContext);

  return (
    <>
    {[false].map((expand) => (
      <Navbar key={expand} expand={expand} className="mb-3">
      <Container fluid>
      <Navbar.Brand href="#" className="spacingletters ">PEN SHOP</Navbar.Brand>
      <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
      <Navbar.Offcanvas
      id={`offcanvasNavbar-expand-${expand}`}
      aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
      placement="end"
      >
      <Offcanvas.Header closeButton>
      <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`} className="spacingletters ">
      PEN SHOP
      </Offcanvas.Title>
      </Offcanvas.Header>
      <Offcanvas.Body>
      <Nav className="justify-content-end flex-grow-1 pe-3">
      <Nav.Link as={NavLink} to="/">Home</Nav.Link>
      { (user.id !== null) ?
        (user.isAdmin === true) ? 
        <>
        <Nav.Link as={NavLink} to="/admin">Admin</Nav.Link>
        <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
        </>
        :
        <>
        <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
        <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
        </>
      :
      <>
      <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
      <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
      </>
    } 
    </Nav>
    </Offcanvas.Body>
    </Navbar.Offcanvas>

    </Container>
    </Navbar>
    ))}
    </>
    );
}


