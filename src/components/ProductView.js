import { useState, useEffect} from 'react';
import { Container, Button, Row, Col, Form, FloatingLabel} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductView() {
	
	const navigate = useNavigate();

	const{productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState("");
	const [totalAmount, setTotalAmount] = useState(0);



	const createOrder = (e)=>{
		e.preventDefault();
		
		fetch(`${process.env.REACT_APP_API_URL}/users/createOrder/${productId}`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				
				productName: name,
				quantity: quantity,
				totalAmount: totalAmount
				
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			if (data.error) {
				Swal.fire({
					title: "purchase failed Failed",
					icon: "error",
					text: data.error,
				});
			} else {
				console.log(name, quantity, totalAmount)
				Swal.fire({
					title: "purchase success",
					icon: "success",
					text: "You have successfully",
				});
				navigate("/products");
				quantity('');
			}
		})
	}

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/retrieveOneProduct/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
		const newTotalAmount = price * quantity;
		setTotalAmount(newTotalAmount);

	},[productId, price, quantity])


	return (

	<Container className="vh-100 d-flex justify-content-center align-items-center ">
	<Container>

		<h1 className="color-gray spacingletters pb-3 text-center">Order Now!</h1>

	<Form onSubmit={(e) => createOrder(e)}>

		<h2>{name}</h2>
		<h6>{description}</h6>

		<Row className="g-2 pb-1">
			<Col md>
			<h4>Price: {price}</h4>
			</Col>

			<Col md>
			<h4>Amount to Pay: {totalAmount}</h4>
			</Col>
		</Row>

		<Row className="g-2 pb-1 mt-1">
			<Col md >
			<FloatingLabel controlId="" label="Enter quantity">
			<Form.Control 
			type="text" 
			placeholder="r"
			value={quantity}
			onChange={e => setQuantity(e.target.value.slice(0, 11))}
			maxLength={11} 
			required
			/>
			</FloatingLabel>
			</Col>

			<Col md>
			</Col>
		</Row>

		<Row className="g-2 pb-1 mt-1">
			<Col md> 
			<p></p>
			</Col>

			<Col md>
			{quantity !== ''  ? 
			<Button  type="submit" id="submitBtn" className="button-properties  w-100" >Buy Now</Button>
			:
			<Button  type="submit" id="submitBtn" className="button-properties  w-100"  disabled>Buy Now</Button>
			}
			</Col>

			<Col md>
			<p></p>
			</Col>
		</Row>

		</Form>

	</Container>
	</Container>

	)
}