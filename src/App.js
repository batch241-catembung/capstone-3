import './App.css';
import { useState, useEffect } from 'react';
import {UserProvider} from './UserContext';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import { Container } from 'react-bootstrap';

//pages
import Admin from './pages/Admin'
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import Register from './pages/Register';

//components
import AppNavbar from './components/AppNavbar';

//admin pages
import CreateProduct from './pages/adminAccess/CreateProduct';
import AllProducts from './pages/adminAccess/AllProducts';

//admin components
import ProductView from './components/ProductView'
import AllProductView from './components/adminAccess/AllProductView'



function App() {

  const [user, setUser] = useState({id: null, isAdmin: null});
  const unsetUser = () => {localStorage.clear();}

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      // User is logged in
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
        // User is logged out
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    }) 
}, [])

  return (
  
    <>
    <UserProvider value={{user, setUser, unsetUser}} >
      <Router >
       <AppNavbar/>
      <Container >
            <Routes>
              <Route path="/" element={<Home/>} />
              <Route path="/register" element={<Register/>} />
              <Route path="/login" element={<Login/>} /> 
              <Route path="/admin" element={<Admin/>} /> 
              <Route path="/products" element={<Products/>} />
              <Route path="/products/:productId" element={<ProductView/>} />
              <Route path="/allproduct" element={<AllProducts/>} />
              <Route path="/allproduct/:productId" element={<AllProductView/>} />
              <Route path="/createproduct" element={<CreateProduct/>} />
              <Route path="/logout" element={<Logout/>} /> 
              <Route path="/*" element={<Error/>} />  
          </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>
  );
}

export default App;

