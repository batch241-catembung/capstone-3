import AllProductCard from '../../components/adminAccess/AllProductCard';
import {useState, useEffect } from 'react';
import {Row, Container} from 'react-bootstrap';

export default function AllProducts(){

	const [allproduct, setallProduct] = useState([]);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/retrieveAllProduct`)
		.then(res=> res.json())
		.then(data =>{
			console.log(data);
			setallProduct(data.map(allproduct=>{
				return(
					<AllProductCard key={allproduct.id} allproduct = {allproduct}/>
					)
			} ))
		})
	}, [] )

	return(
		<>
		<Container >
		<Row >
		{allproduct}
		</Row>
		</Container>
		</>
		)
}