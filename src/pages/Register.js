import {useState, useEffect, useContext} from 'react';
import {useNavigate, Navigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import {Form, Button, Container, FloatingLabel, Row, Col} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Register() {

    const {user} = useContext(UserContext);
    const navigate = useNavigate();

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [contactNo, setContactNo] = useState('');
    const [address, setAddress] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);



    function registerUser(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email    
            }),
        })
        .then(res=>res.json())
        .then(data=>{
            console.log(data);
            if(data===true){

                Swal.fire({
                  title: "Email already exist",
                  icon: "error",
                  text: "Please provide different email to register."
              })
                
            } else {

                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        name: name,
                        email: email,
                        contactNo:contactNo,
                        address: address,
                        password:password1
                    })
                })
                .then(res=>res.json())
                .then(data=>{
                    console.log(data);
                    
                    if (data.error) {
                        Swal.fire({
                          title: "Registration Failed",
                          icon: "error",
                          text: data.error,
                      });
                    } else {
                        Swal.fire({
                          title: "Successfully registered",
                          icon: "success",
                          text: "You have successfully registered to Zuitt.",
                      });
                        navigate("/login");
                    }
                    

                })

            }

        })

    }

    useEffect(() => {

        if((name !== '' && 
            email !== '' && 
            contactNo !== '' && 
            address !== '' && 
            password1 !== '' && 
            password2 !== '') 
            && (password1 === password2))
        {
            setIsActive(true);
        } 
        else{
            setIsActive(false);
        }

    }, [name, email, contactNo, address, password1, password2]);

    return (
        (user.id !== null) ?
        <Navigate to ="/products" />

        :

        <Form onSubmit={(e) => registerUser(e)}>

        <Container className="vh-100 d-flex justify-content-center align-items-center text-center">
        <Container >

        <h1 className="color-gray spacingletters pb-3">Join us!</h1>

        <Row className="g-2 pb-1">
            <Col md>
            <FloatingLabel controlId="" label="Full Name">
            <Form.Control type="text"
            placeholder="Enter your Full name"
            value={name}
            onChange={e => setName(e.target.value)}
            onKeyPress={(event) => {if (!/[a-zA-Z ]/.test(event.key)) {event.preventDefault();}}}
            required    />
            </FloatingLabel>
            </Col>

            <Col md>
            <FloatingLabel controlId="" label="Email address">
            <Form.Control 
            type="email" 
            placeholder="Enter email"
            value={email}
            onChange={e => setEmail(e.target.value)} 
            required
            />
            </FloatingLabel>
            </Col>
        </Row>

        
        <Row className="g-2 pb-1">
            <Col md>
            <FloatingLabel controlId="" label="Mobile Number">
            <Form.Control 
            type="text" 
            placeholder="Enter Mobile Number"
            value={contactNo}
            onChange={e => setContactNo(e.target.value.slice(0, 11))}
            onKeyPress={(event) => {if (!/[0-9]/.test(event.key)) {event.preventDefault();}}}
            maxLength={11} 
            required
            />
            </FloatingLabel>
            </Col>

            <Col md>
            <FloatingLabel controlId="" label="Address">
            <Form.Control 
            type="text" 
            placeholder="Enter first name"
            value={address}
            onChange={e => setAddress(e.target.value)} 
            required
            />
            </FloatingLabel>
            </Col>
        </Row>

        <Row className="g-2 pb-1">
            <Col md>
            <FloatingLabel controlId="" label="Password">
            <Form.Control 
            type="password" 
            placeholder="Password" 
            value={password1}
            onChange={e => setPassword1(e.target.value)}
            required
            />
            </FloatingLabel>
            </Col>

            <Col md>
            <FloatingLabel controlId="" label="Retype Password">
            <Form.Control 
            type="password" 
            placeholder="Verify Password" 
            value={password2}
            onChange={e => setPassword2(e.target.value)}
            required
            />
            </FloatingLabel>
            </Col>
        </Row>


        <Row className="g-2 pb-1">
            <Col md> 
            <p></p>
            </Col>

            <Col md>
            { isActive ?
            <Button variant="primary" type="submit" id="submitBtn" className="button-properties  w-100">
            Register
            </Button>
            :
            <Button variant="primary" type="submit" id="submitBtn" disabled className="button-properties w-100">
            Register
            </Button>
            }
            </Col>

            <Col md>
            <p></p>
            </Col>
        </Row>

    <h6 className=" pt-3 color-gray">Already have an account
    <Link as={Link} to="/login" className="font-weight-bold text-decoration-none color-red "> Click here </Link>
    to go login </h6>

    </Container>
    </Container>
    
    </Form>

    )
}

