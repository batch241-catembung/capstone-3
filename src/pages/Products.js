import { useState, useEffect } from 'react';
import { Row, Container } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';

export default function Products() {

  const [product, setProduct] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/retrieveAllActiveProduct`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        setProduct(data.map(product => {
          return (
            <ProductCard key={product.id} product={product} />
          )
        }))
      })
  }, [])

  return (
    <>
    <Container >
      <Row >
        {product}
      </Row>
    </Container>
    </>
  )
}
