import { useState, useEffect, useContext } from 'react';
import {Navigate, Link} from 'react-router-dom';
import {Form, Button, Row, Container, Col, FloatingLabel} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(){

    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    function authenticate(e){

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);


            if(typeof data.access !== "undefined"){

                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to P E N  S H O P!"
                })
            } else {
                Swal.fire({
                    title: "Somethings wrong!",
                    icon: "error",
                    text: "The email or mobile number you entered isn’t connected to an account."
                })
            }
        });
        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {

        if(email !== '' && password !==''){
            setIsActive(true);
        } else{
            setIsActive(false);
        }
    }, [email, password])

    return (
        (user.id !== null) ?
        <Navigate to ="/"/>
        :
        <Form onSubmit={(e) => authenticate(e)}>

        <Container className="vh-100 d-flex justify-content-center align-items-center text-center">
        <Container>
        
        <h1 className="color-gray spacingletters pb-3">Shop Now!</h1>
        
        <Row className="g-2 pb-1">

            <Col md>
            <FloatingLabel controlId="" label="email">
            <Form.Control 
            type="email" 
            placeholder=" " 
            value={email}
            onChange={e => setEmail(e.target.value)}
            required
            />
            </FloatingLabel>
            </Col>

            <Col md>
            <FloatingLabel controlId="" label=" Password">
            <Form.Control 
            type="password" 
            placeholder="Verify Password" 
            value={password}
            onChange={e => setPassword(e.target.value)}
            required
            />
            </FloatingLabel>
            </Col>

        </Row>

            <Row className="g-2 pb-1 pt-3">

            <Col md> 
            <p></p>
            </Col>

            <Col md>
            { isActive ?
            <Button variant="primary" type="submit" id="submitBtn" className="button-properties  w-100">
            Login
            </Button>
            :
            <Button variant="primary" type="submit" id="submitBtn" disabled className="button-properties w-100">
            Login
            </Button>
            }
            </Col>

            <Col md>
            <p></p>
            </Col>

        </Row>

    <h6 className=" pt-3 color-gray">Don't have an account yet? 
    <Link as={Link} to="/register" className="font-weight-bold text-decoration-none color-red "> Click here </Link>
    to register. 
    </h6>

    </Container>
    </Container>

    </Form>
    )
}