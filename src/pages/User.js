import {useState, useEffect} from 'react';
import UserOrderCard from '../../components/UserAccess/UserOrderCard';
import { Row, Col, Container} from 'react-bootstrap';
export default function UserOrder(){

	const [allOrder, setAllOrder] = useState([]);
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/users/retrievemyorders`)
		.then(res=> res.json())
		.then(data =>{
			console.log(data);
			setallProduct(data.map(allOrder=>{
				return(<UserOrderView key={allOrder.id} allOrder = {allOrder}/>)
			} ) )
		} )
	}, [] )

	return(
		<>
		<Container >
			<Row >
			{allOrder}
			</Row>
		</Container>

		</>
		)
}