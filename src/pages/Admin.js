import AllProducts from './adminAccess/AllProducts';
import {Button, Row, Col, Container} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';

export default function Admin() {

	return (
		<>
		<Row className="g-2 pb-1">

			<Col md>
			</Col>

			<Col md>
			<Container className="d-flex justify-content-center">
			<Button  as={NavLink} to="/createproduct" variant="primary" className="button-properties">create product</Button>
			</Container>
			</Col>

			<Col md>
			</Col>

		</Row> 

		<AllProducts/>
		</>
	
	)
}
